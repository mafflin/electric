using Toybox.System;

class Phone extends Icon {
    function initialize() {
        Icon.initialize(Rez.Drawables.Phone);
    }

    function on() {
        return System.getDeviceSettings().phoneConnected;
    }
}
