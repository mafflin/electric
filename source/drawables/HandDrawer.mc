using Toybox.System;
using Toybox.Graphics;

module HandDrawer {
    const ZERO_POSITION = 90;
    const RADIUS = System.getDeviceSettings().screenWidth / 2;

    function draw(dc, value, color, handWidth, handLength) {
        var position = ZERO_POSITION - value;

        dc.setPenWidth(handLength);
        dc.setColor(color, Graphics.COLOR_TRANSPARENT);
        dc.drawArc(
            RADIUS,
            RADIUS,
            RADIUS - handLength / 2,
            Graphics.ARC_CLOCKWISE,
            position + handWidth / 2,
            position - handWidth / 2
        );
    }
}
