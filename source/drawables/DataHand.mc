using Toybox.WatchUi;
using Toybox.System;
using Toybox.Graphics;
using Toybox.ActivityMonitor;

class DataHand extends WatchUi.Drawable {
    private const RATIO = 360;

    private var primaryColor;
    private var secondaryColor;
    private var enabled;
    private var handLength;
    private var handWidth;

    function initialize(params) {
        Drawable.initialize(params);

        handlSettingUpdate(Application.getApp());
    }

    function handlSettingUpdate(app) {
        enabled = app.getProperty("ShowDataHand");
        primaryColor = app.getProperty("DataHandColor");
        secondaryColor = app.getProperty("DataHandSecondaryColor");
        handLength = app.getProperty("DataHandLength");
        handWidth = app.getProperty("DataHandWidth");
    }

    function draw(dc) {
        if (!enabled) { return; }

        var info = ActivityMonitor.getInfo();
        var steps = info.steps;
        var stepGoal = info.stepGoal;

        if (!steps) { return; }
        if (!stepGoal) { return; }

        var color = (steps > stepGoal) ? secondaryColor : primaryColor;
        var position = (RATIO * steps / stepGoal).toNumber();

        HandDrawer.draw(dc, position, color, handWidth, handLength);
    }
}
