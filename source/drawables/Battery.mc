using Toybox.WatchUi;
using Toybox.System;

class Battery {
    private var resources = {};
    private var images = [
        Rez.Drawables.Battery0,
        Rez.Drawables.Battery10,
        Rez.Drawables.Battery20,
        Rez.Drawables.Battery30,
        Rez.Drawables.Battery40,
        Rez.Drawables.Battery50,
        Rez.Drawables.Battery60,
        Rez.Drawables.Battery70,
        Rez.Drawables.Battery80,
        Rez.Drawables.Battery90,
        Rez.Drawables.Battery100,
    ];

    function initialize() {
        for (var i = 0; i < images.size(); i++) {
            var key = generateKey(i * 10);
            var resource = WatchUi.loadResource(images[i]);

            resources.put(key, resource);
        }
    }

    function on() {
        return true;
    }

    function draw(dc, x, y) {
        var battery = System.getSystemStats().battery.toNumber();
        var key = generateKey(battery);

        dc.drawBitmap(x, y, resources[key]);
    }

    private function generateKey(level) {
        var value = level / 10 * 10;
        return Lang.format("$1$_$2$", [ "level", value ]);
    }
}
