using Toybox.Activity;

class Gps extends Icon {
    function initialize() {
        Icon.initialize(Rez.Drawables.Gps);
    }

    function on() {
        return Activity.getActivityInfo().timerTime > 0;
    }
}
