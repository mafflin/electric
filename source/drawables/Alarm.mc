using Toybox.System;

class Alarm extends Icon {
    function initialize() {
        Icon.initialize(Rez.Drawables.Alarm);
    }

    function on() {
        return System.getDeviceSettings().alarmCount > 0;
    }
}
