using Toybox.WatchUi;
using Toybox.System;
using Toybox.Graphics;

class StatusBar extends WatchUi.Drawable {
    private const RELATIVE_Y = 0.15;

    private var icons = [];
    private var enabled;

    function initialize(params) {
        Drawable.initialize(params);
        icons = [
            new Battery(),
            new Phone(),
            new Alarm(),
            new Dnd(),
            new Gps(),
            new Notifications(),
        ];

        handlSettingUpdate(Application.getApp());
    }

    function handlSettingUpdate(app) {
        enabled = app.getProperty("ShowStatusBar");
    }

    function draw(dc) {
        if (!enabled) { return; }

        var visible = visibleIcons();
        var total = visible.size();
        var y = dc.getHeight() * RELATIVE_Y;
        var start = (dc.getWidth() - (3 * total - 1) * Icon.WIDTH / 2) / 2;

        for (var i = 0; i < total; i++) {
            var x = (start + (i * 1.5 * Icon.WIDTH)).toNumber();
            visible[i].draw(dc, x, y);
        }
    }

    private function visibleIcons() {
        var visible = [];

        for (var i = 0; i < icons.size(); i++) {
            var icon = icons[i];
            if (icon.on()) { visible.add(icon); }
        }

        return visible;
    }
}
