using Toybox.System;
using Toybox.Graphics;

class Notifications extends Icon {
    function initialize() {
        Icon.initialize(Rez.Drawables.Notifications);
    }

    function on() {
        return System.getDeviceSettings().notificationCount > 0;
    }

    function draw(dc, x, y) {
        dc.drawBitmap(x, y, resource);
    }
}
