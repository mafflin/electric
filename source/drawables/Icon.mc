using Toybox.WatchUi;

class Icon {
    static const WIDTH = 24;
    static const HEIGHT = 24;

    protected var resource;

    function initialize(rez) {
        resource = WatchUi.loadResource(rez);
    }

    function on() {
        return true;
    }

    function draw(dc, x, y) {
        dc.drawBitmap(x, y, resource);
    }
}
