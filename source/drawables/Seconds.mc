using Toybox.WatchUi;
using Toybox.System;
using Toybox.Graphics;

class Seconds extends WatchUi.Drawable {
    private const RATIO = 6;

    private var lowEnergyMode = false;
    private var color;
    private var enabled;
    private var handLength;
    private var handWidth;

    function initialize(params) {
        Drawable.initialize(params);

        handlSettingUpdate(Application.getApp());
    }

    function handlSettingUpdate(app) {
        enabled = app.getProperty("ShowSeconds");
        color = app.getProperty("SecondsColor");
        handLength = app.getProperty("SecondsHandLength");
        handWidth = app.getProperty("SecondsHandWidth");
    }

    function setMode(mode) {
        lowEnergyMode = mode;
    }

    function draw(dc) {
        if (lowEnergyMode || !enabled) { return; }

        var position = System.getClockTime().sec * RATIO;

        HandDrawer.draw(dc, position, color, handWidth, handLength);
    }
}
