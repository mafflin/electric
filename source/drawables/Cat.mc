using Toybox.Application;
using Toybox.WatchUi;

class Cat extends WatchUi.Drawable {
    private const RELATIVE_Y = 0.75;

    private var resource;
    private var enabled;

    function initialize(params) {
        Drawable.initialize(params);

        resource = WatchUi.loadResource(Rez.Drawables.Cat);

        handlSettingUpdate(Application.getApp());
    }

    function handlSettingUpdate(app) {
        enabled = app.getProperty("ShowCat") && !app.getProperty("ShowDate");
    }

    function draw(dc) {
        if (!enabled) { return; }

        var x = (dc.getWidth() - Icon.WIDTH) / 2;
        var y = dc.getHeight() * RELATIVE_Y;

        dc.drawBitmap(x, y, resource);
    }
}
