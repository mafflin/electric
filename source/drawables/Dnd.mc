using Toybox.System;

class Dnd extends Icon {
    function initialize() {
        Icon.initialize(Rez.Drawables.Dnd);
    }

    function on() {
        var settings = System.getDeviceSettings();
        var version = settings.monkeyVersion;

        if (version[0] < 2) { return false; }

        return settings.doNotDisturb;
    }
}
