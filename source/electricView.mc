using Toybox.WatchUi;

class ElectricView extends WatchUi.WatchFace {
    var timeContainer;
    var dateContainer;
    var secondsContainer;
    var dataHandContainer;
    var statusBarContainer;
    var catContainer;

    var lowEnergyMode = false;
    var time;
    var date;

    function initialize() {
        WatchFace.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.WatchFace(dc));

        timeContainer = View.findDrawableById("Time");
        dateContainer = View.findDrawableById("Date");
        secondsContainer = View.findDrawableById("Seconds");
        dataHandContainer = View.findDrawableById("DataHand");
        statusBarContainer = View.findDrawableById("StatusBar");
        catContainer = View.findDrawableById("Cat");

        time = new ElectricTime();
        date = new ElectricDate();

        handlSettingUpdate();
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
        timeContainer.setText(time.get());
        dateContainer.setText(date.get());
        secondsContainer.setMode(lowEnergyMode);

        View.onUpdate(dc);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
        lowEnergyMode = false;
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
        lowEnergyMode = true;

        WatchUi.requestUpdate();
    }

    function handlSettingUpdate() {
        var app = Application.getApp();

        date.handlSettingUpdate(app);
        time.handlSettingUpdate();

        statusBarContainer.handlSettingUpdate(app);
        secondsContainer.handlSettingUpdate(app);
        dataHandContainer.handlSettingUpdate(app);
        catContainer.handlSettingUpdate(app);
    }
}
