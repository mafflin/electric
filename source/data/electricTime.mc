using Toybox.System;
using Toybox.Lang;

class ElectricTime {
  var is24Hour;
  var format = "$1$:$2$";

  function initialize() {
    handlSettingUpdate();
  }

  function handlSettingUpdate() {
    is24Hour = System.getDeviceSettings().is24Hour;
  }

  function get() {
    var time = System.getClockTime();
    var hours = (!is24Hour && time.hour > 12) ? time.hour - 12 : time.hour;

    return Lang.format(
      format,
      [
        hours,
        time.min.format("%02d"),
      ]
    );
  }
}
