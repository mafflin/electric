using Toybox.Application;
using Toybox.Lang;
using Toybox.Time;
using Toybox.Time.Gregorian;

class ElectricDate {
  const FORMAT_SHORT = "$1$ $2$";
  const FORMAT_LONG = "$1$ $2$ $3$";

  var enabled;
  var format;

  function initialize() {
    handlSettingUpdate(Application.getApp());
  }

  function handlSettingUpdate(app) {
    enabled = app.getProperty("ShowDate");
    format = app.getProperty("DateLongFormat") ? FORMAT_LONG : FORMAT_SHORT;
  }

  function get() {
    if (!enabled) { return ""; }

    var today = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
    return Lang.format(
      format,
      [
        today.day_of_week,
        today.day,
        today.month
      ]
    );
  }
}
